const http = require('http');
const fs = require('fs');
const { v4: uuidv4 } = require("uuid");


const server = http.createServer( (request, response) => {
    const url = request.url;
    const splitUrl = url.split("/");
    const id = splitUrl[splitUrl.length - 1];
    const endPoint = splitUrl[splitUrl.length - 2];

    if(request.method === 'GET' && request.url === '/html'){
        response.setHeader('Content-Type', 'text/html');

        fs.readFile('./index.html', 'utf-8', (error, data) => {
            if(error){
                response.statusCode = 500;
                response.end("Internal Server Error");
                console.log(error);
            }
            else{
                response.end(data);
            }
        })
    }
    else if(request.method === "GET" && request.url === '/json'){
        response.setHeader('Content-Type', 'application/json');

        fs.readFile('./demo.json', 'utf-8', (error,data) => {
            if(error){
                response.statusCode = 500;
                response.end('Internal Server Error');
                console.log(error);
            }
            else{
                response.end(data);
            }
        });
    }
    else if(request.method === 'GET' && request.url === '/uuid'){
        const generatUuid = uuidv4();
        const JsongUuid = {
            uuid : generatUuid,
        };

        response.setHeader('Content-Type', 'application/json');
        response.write(JSON.stringify(JsongUuid));
        response.end();
    }
    else if (request.method === "GET" && endPoint === "status"){
        if(id >= 100 && id <= 600){
            response.setHeader('Content-Type', 'text/html');
            response.write(`Response code : ${id}`);
            response.end();
        }
        else{
            response.setHeader('Content-Type', 'text/html');
            response.write('Response code not valid');
            response.end();
        }
    }
    else if(request.method === 'GET' && endPoint === 'delay'){
        setTimeout( () => {
            response.writeHead(200, 'Content-Type', 'text/html');
            response.write(`Success after ${id} seconds`);
            response.end();
        }, id * 1000);
    }
    else {
        response.statusCode = 404;
        response.end('Not Found');
    }
});

const hostname = '127.0.0.1';
const port = 3000;

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});